using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MyAniConScript : MonoBehaviour
{
    private Animator myAnimator;
    private float multiplicateur_vSpeed = 1.0f;
    private float multiplicateur_hSpeed = 1.0f;

    void Start()
    {
        myAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            multiplicateur_vSpeed = Mathf.Min(multiplicateur_vSpeed + 0.03f, 2.0f);
        }
        else
        {
            multiplicateur_vSpeed = Mathf.Max(multiplicateur_vSpeed - 0.03f, 1.0f);
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            multiplicateur_hSpeed = Mathf.Min(multiplicateur_hSpeed + 0.06f, 2.0f);
        }
        else
        {
            multiplicateur_hSpeed = Mathf.Max(multiplicateur_hSpeed - 0.06f, 1.0f);
        }

        float vSpeed = 0f;
        float hSpeed = 0f;

        if (Input.GetKey(KeyCode.Z))
        {
            vSpeed = 1f * multiplicateur_vSpeed;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            vSpeed = -1f * multiplicateur_vSpeed;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            hSpeed = -1f * multiplicateur_hSpeed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            hSpeed = 1f * multiplicateur_hSpeed;
        }

        myAnimator.SetFloat("vSpeed", vSpeed);
        myAnimator.SetFloat("hSpeed", hSpeed);
    }
}
