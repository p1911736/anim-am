// ===========================================================================================================================
// ============================= IKJoint =====================================================================================
// ===========================================================================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class IKJoint
{
    // la position modifiée par l'algo : en fait la somme des positions des sous-branches. 
    // _weight comptera le nombre de sous-branches ayant touchées cette articulation.
    private Vector3 _position;
    
    // un lien vers le Transform de l'arbre d'Unity
    private Transform _transform;
    
    // un poids qui indique combien de fois ce point a été bougé par l'algo.
    public float _weight = 0.0f;   


    public string name
    {
        get
        {
            return _transform.name;
        }
    }

    public Vector3 position     // la position moyenne
    {
        get
        {
            if (_weight == 0.0f) return _position;
            else return _position / _weight;
        }
    }

    public Vector3 positionTransform
    {
        get
        {
            return _transform.position;
        }
    }

    public Transform transform
    {
        get
        {
            return _transform;
        }
    }

    public Vector3 positionOrigParent
    {
        get
        {
            return _transform.parent.position;
        }
    }

	public IKJoint(Transform t)
    {
		_weight = 0.0f;
        _transform = t;
        _position = _transform.position;
    }

    public void SetPosition(Vector3 p)
    {
		_weight = 0.0f;
		_position = p;
    }

    public void AddPosition(Vector3 p)
    {
		if (_weight == 0.0f)
		{
			_position = p;
		} else {
	        _position += p;
		}
        _weight += 1.0f;

    }

    public void ToTransform()
    {
        _weight = 0.0f;
		_transform.position = position;
		_position = _transform.position;
    }
    
    public void Solve(IKJoint anchor, float l)
    {
        Vector3 anchorToJoint = _position - anchor.position;
        Vector3 anchorToJointScaled = anchorToJoint.normalized * l;
        AddPosition(_position - anchorToJoint + anchorToJointScaled);
    }
}

