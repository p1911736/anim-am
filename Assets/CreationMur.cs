using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreationMur : MonoBehaviour
{
	public GameObject brique;
    public int largeur = 0;
    public int hauteur = 0;

    void Start()
    {
        for (int i = 0; i < largeur; i++)
  		{
            for (int j = 0; j < hauteur; j++)
            {
                Instantiate(brique, new Vector3(i-10, j, 20), Quaternion.identity, transform);
            }
        }
    }
}


