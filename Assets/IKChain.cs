// ===========================================================================================================================
// =========================================== IKChain =======================================================================
// ===========================================================================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class IKChain
{
    // Quand la chaine comporte une cible pour la racine. 
    // Ce sera le cas que pour la chaine comportant le root de l'arbre.
    private Transform rootTarget = null;                              
    
    // Quand la chaine à une cible à atteindre, 
    // ce ne sera pas forcément le cas pour toutes les chaines.
    private Transform endTarget = null;

    // Toutes articulations (IKJoint) triées de la racine vers la feuille. N articulations.
    public List<IKJoint> joints = new List<IKJoint>();             
    
    // Contraintes pour chaque articulation : la longueur (à modifier pour 
    // ajouter des contraintes sur les angles). N-1 contraintes.
    private List<float> constraints = new List<float>();    

    
    // Un cylndre entre chaque articulation (Joint). N-1 cylindres.
    //private List<GameObject> cylinders = new List<GameObject>();    

    // Créer une chaine composée uniquement d'une racine.
    public IKChain(IKJoint _rootNode, Transform _rootTarget)
    {
        rootTarget = _rootTarget;
        joints.Add(_rootNode);
    }

	public void AddNode(IKJoint node)
    {
        Vector3 lastToCurrent = node.transform.position - Last().transform.position;
        constraints.Add(lastToCurrent.magnitude);
        joints.Add(node);
    }

	public void SetTarget(Transform target)
	{
		endTarget = target;
	}

    public IKJoint First()
    {
        return joints[0];
    }

    public IKJoint Last()
    {
        return joints[ joints.Count-1 ];
    }
    
    public void Backward()
    {
        if (endTarget != null && joints.Count > 0)
        {
            joints[joints.Count - 1].SetPosition(endTarget.position);
		}
        for (int i = joints.Count - 2; i >= 0; i--)
        {
			
          	joints[i].Solve(joints[i + 1], constraints[i]);
		}
        
    }
    
    public void Forward()
    {
        if (rootTarget != null && joints.Count > 0)
        {
            joints[0].SetPosition(rootTarget.position);
		}
        for (int i = 1; i < joints.Count; i++)
        {
			joints[i].Solve(joints[i - 1], constraints[i - 1]);
        }
    }   
	
	public void updateChaine()
	{
		for(int i=0; i<joints.Count; i++)
		{
			joints[i].SetPosition(joints[i].position);
		}
	}

    public void ToTransform()
    {
	    for (int i = 1; i < joints.Count - 1; i++)
	    {
		    joints[i].ToTransform();
		    Vector3 vectorToNextJoint = joints[i + 1].position - joints[i].transform.position;
			joints[i].transform.LookAt(joints[i+1].position);
		    Quaternion rotation = Quaternion.FromToRotation(joints[i].transform.up, vectorToNextJoint);
		    joints[i].transform.rotation = joints[i].transform.rotation * rotation;
	    }
	    joints[joints.Count - 1].ToTransform();
    }

    public void Check()
    {
        Debug.Log("check");
        foreach (IKJoint joint in joints)
        {
            Debug.Log(joint.name);
        }
    }
}


