// ==============================================
// ================= IK =========================
// ==============================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class IK : MonoBehaviour
{
    // Le transform (noeud) racine de l'arbre, 
    // le constructeur créera une sphère sur ce point pour en garder une copie visuelle.
    public GameObject rootNode = null;
    private GameObject rootTarget = null;
    
    // Le transform (noeud) cible pour srcNode
    public Transform targetNode = null;

    // Si vrai, recréer toutes les chaines dans Update
    public bool createChains = true;                            
    
    // Toutes les chaines cinématiques 
    public List<IKChain> chains = new List<IKChain>();          

    void Start()
    {
        if (createChains)
        {
            createChains = false;
            rootTarget = new GameObject("rootTarget");
            rootTarget.transform.position = rootNode.transform.position;
            findChains();
			chains[1].SetTarget(GameObject.Find("endTargetA").transform);
			chains[4].SetTarget(GameObject.Find("endTargetB").transform);
			chains[3].SetTarget(GameObject.Find("endTargetC").transform);
		}
    }

    void Update()
    {
        if (createChains)
            Start();

        IKOneStep(true);
        
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Chains count="+chains.Count);
            foreach (IKChain ch in chains)
                ch.Check();
        }
    }

    void findChains()
    {
	    for(int i=0; i<rootNode.transform.childCount; i++)
	    {
		    IKJoint chaine_root = new IKJoint(rootNode.transform);
		    IKChain chaine = new IKChain(chaine_root, rootTarget.transform);
		    chains.Add(chaine);
		    createTree(i,i);
	    }
    }
	
	void IKOneStep(bool down)
    {
	    // On traverse l'arbre de chaines à l'envers pour le backward pour déplacer les
	    // racines des chaines plus hautes avant de déplacer les chaines plus basses
		for(int i=chains.Count-1; i >= 0; i--){
			chains[i].Backward();
		}
		foreach (IKChain chaine in chains)
        {
			chaine.updateChaine();	// sauvegarde les positions des éléments sans affecter les transforms
        }
		foreach (IKChain chaine in chains)
        {
            chaine.Forward();
        }
		foreach (IKChain chaine in chains)
        {
        	chaine.ToTransform();	// met à jour les transforms
        }

    }

    private void createTree(int indiceChaine, int indiceFils)
    {
	    Transform last = chains[indiceChaine].Last().transform;
		Transform child = last.GetChild(indiceFils);
		// Tant qu'une intersection ou une feuille n'est pas atteinte, on ajoute les noeuds
		while (child.childCount == 1)
        {
            chains[indiceChaine].AddNode(new IKJoint(child));
            child = chains[indiceChaine].Last().transform.GetChild(0);
        }
		// Si le dernier noeud est une feuille, on l'ajoute
        if (child.childCount == 0)
        {
	        chains[indiceChaine].AddNode(new IKJoint(child));
        } 
        else // Si c'est une intersection, on doit créer d'autres chaines
        { 
	        IKJoint intersection = new IKJoint(child);
	        chains[indiceChaine].AddNode(intersection);
	        for (int i = 0; i < child.childCount ; i++)
	        {
		        chains.Add(new IKChain(intersection, null));
		        int indiceProchaineChaine = chains.Count - 1;
		        createTree(indiceProchaineChaine, i);
	        }
		}
    }

}


